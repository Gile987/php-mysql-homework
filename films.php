<?php
echo "<h2>Films</h2>";
/* 
select ALL data from the table "films"
SELECT = what to select 
FROM = which table (films)
WHERE = conditions to satisfy
*/
$sql = "SELECT * FROM films";
$result = mysqli_query($connection,$sql) or die(mysqli_error($connection));

// create the table if the number of rows is greater than 0
if(mysqli_num_rows($result)>0)
{
    // https://phppot.com/php/php-escape-sequences/
    echo "<table class=\"tabela\">
        <tr>
            <th>id</th>
            <th>title</th>
            <th>id_genre</th>
            <th>time</th>
            <th>description</th>
        </tr>";
    // fetch the array, echo it in a table - result type = MYSQLI_ASSOC (associative array)
    while ($row=mysqli_fetch_array($result,MYSQLI_ASSOC)) 
    {
        $rowID = $row["id"];
        /* 
        row ID must be clickable
        hence: https://stackoverflow.com/questions/15801173/how-to-make-search-query-results-show-up-as-links-to-their-pages
        also: https://stackoverflow.com/questions/34315568/how-to-display-search-results-in-one-table-with-php-mysql
        */
        echo "<tr>
                <td><a href=\"update.php?filmID=$rowID\">$rowID</a></td>
                <td>".$row["title"]."</td>
                <td>".$row["id_genre"]."</td>
                <td>".$row["time"]."</td>
                <td>".$row["description"]."</td>
            </tr>";
    }
    echo "</table><br>";
    mysqli_free_result($result);
}
mysqli_close($connection);