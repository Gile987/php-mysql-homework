<?php
echo "<h2>Genres</h2>";

$sql = "SELECT * FROM genre";
$result = mysqli_query($connection,$sql) or die(mysqli_error($connection));

// check out films.php for an explanation
if(mysqli_num_rows($result)>0)
{
    echo "<table class=\"tabela\">
            <tr>
                <th>id</th>
                <th>genre</th>
            </tr>";
    while ($row=mysqli_fetch_array($result,MYSQLI_ASSOC))
    {
        echo "<tr>
                <td>".$row["id"]."</td>
                <td>".$row["genre"]."</td>
              </tr>";
    }
    echo "</table><br>";
    mysqli_free_result($result);
}
mysqli_close($connection);