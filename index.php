<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" type="text/css" href="style/style.css">
<link rel="stylesheet" type="text/css" href="https://necolas.github.io/normalize.css/8.0.0/normalize.css">
<title>PHP and MySQL</title>
</head>
<body>
    
<?php
include("header.html");
include("menu.html");
define("SECRET","@3eweHjdsdfuihjhjhj#VGVgggg!");
// require = halt the script instead of allowing it to continue - http://php.net/manual/en/function.require.php
require('inc/db_config.php');

if(!isset($_GET["link"]))
    $_GET["link"]="index";
switch ($_GET["link"])
{
    case "films";
    include("films.php");
    break;

    case "genre";
    include("genre.php");
    break;

    case "schedule";
    include("schedule.php");
    break;

    case "task";
    include("task.html");
    break;

    default:
    include("films.php");
    break;
}
include("footer.html");
?>