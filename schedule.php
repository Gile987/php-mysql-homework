<?php
echo "<h2>Release Dates</h2>";
// 1. select ALL from "schedule"
$sql = "SELECT * FROM schedule";
// 2. store it in a result set
$result = mysqli_query($connection,$sql) or die(mysqli_error($connection));

if(mysqli_num_rows($result)>0)
{
    echo "<table class=\"tabela\">
        <tr>
            <th>id</th>
            <th>date</th>
            <th>film_id</th>
        </tr>";
    // 1. fetch a result row as an associative array (MYSQLI_ASSOC)
    while ($row=mysqli_fetch_array($result,MYSQLI_ASSOC)) 
    {
        // 2. echo the results in a table (films.php for more info)
        echo "<tr>
                <td>".$row["id"]."</td>
                <td>".$row["date"]."</td>
                <td>".$row["film_id"]."</td>
              </tr>";
    }
    echo "</table><br>"; 
    // free the memory associated with the result
    mysqli_free_result($result);
}
mysqli_close($connection);