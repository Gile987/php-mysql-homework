<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" type="text/css" href="style/style.css">
<link rel="stylesheet" type="text/css" href="https://necolas.github.io/normalize.css/8.0.0/normalize.css">
<title>PHP and MySQL</title>
</head>
<body>

<?php
    include("header.html");
    include("menu.html");
    define("SECRET","@3eweHjdsdfuihjhjhj#VGVgggg!");
    require('inc/db_config.php');

    if(isset($_GET["filmID"]))
    $IDfilm = $_GET["filmID"];
    // 1. select data related to the selected ID of the film chosen on films.php (filmID=1, filmID=2, etc.)
    $sql = "SELECT * FROM films WHERE id = $IDfilm";
    // 2. store it in a result set
    $result = mysqli_query($connection,$sql) or die(mysqli_error($connection));

    if(mysqli_num_rows($result)>0)
    {
        // fetch a result row as an associative array (MYSQLI_ASSOC)
        while ($row=mysqli_fetch_array($result,MYSQLI_ASSOC)) 
        {
            $rowID = $row["id"];
            $rowTitle = $row["title"];
            $rowIDGenre = $row["id_genre"];
            $rowTime = $row["time"];
            $rowDescription = $row["description"];
        }
        // free the memory associated with the result
        mysqli_free_result($result);
    }
    // close a previously opened database connection
    mysqli_close($connection);
?>

<h2>Update Film - ID <?php echo $rowID; ?></h2>

<!-- Use the form to update the database with new info, but echo the old info from the database -->
<form action="realUpdate.php" method="POST">
    <label>Film Title</label><br>
    <input type="text" name="title" value="<?php echo $rowTitle; ?>">
    <br><br>
    <label>Genre</label><br>
    <input type="hidden" name="id_genre" value="<?php echo $rowIDGenre; ?>">

    <?php
    // Use the switch statement to echo the genre of the film depending on the $rowIDGenre value
    switch ($rowIDGenre) {
        case "1":
            echo "<div class=\"genre\">Horror</div>";
            break;
        case "2":
            echo "<div class=\"genre\">Animation</div>";
            break;
        case "3":
            echo "<div class=\"genre\">Comedy</div>";
            break;
        case "4":
            echo "<div class=\"genre\">Drama</div>";
            break;
        default:
            echo "<div class=\"genre\">Action</div>";
    }
    ?> 

    <br>
    <label>Time</label><br>
    <input type="text" name="time" value="<?php echo $rowTime; ?>">

    <?php
    // echo $rowTime in an hours:minutes format
    // round the value - http://php.net/manual/en/function.floor.php
    $hours = floor($rowTime / 60);
    $minutes = $rowTime % 60;
    if($hours > 1 && $minutes != 1){
        echo "$hours hours $minutes minutes";
    }
    elseif($hours > 1 && $minutes == 1){
        echo "$hours hours $minutes minute";
    }
    elseif($hours == 1 && $minutes == 1){
        echo "$hours hour $minutes minute";
    }
    elseif($hours < 1){
        echo "$minutes minutes";
    }
    else
        echo "$hours hour $minutes minutes";
    ?>
    
    <br><br>
    <label>Description</label><br>
    <textarea name="description" rows="5" cols="50"><?php echo $rowDescription; ?></textarea><br><br>
    <input type="hidden" name="film_id" value="<?php echo $rowID; ?>">
    <input type="submit" value="Update DB">
</form>

<?php include("footer.html"); ?>